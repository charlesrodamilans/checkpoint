#!/usr/bin/python3

import subprocess
import time
import re
import os
import socket
import statistics
import glob
import shutil
import distutils
#from distutils import dir_util
from src import npb
import time
import sys

def merge_dict(a, b):
	temp = []
	for machine in b:
		if machine in a:
			for process in b.get(machine):
				if process in a.get(machine):
					aux = []
					aux.extend(b.get(machine).get(process))
					aux.extend(a.get(machine).get(process))
					a.get(machine).update({process: aux})
					temp.append(process)
			for item in temp:
				b.get(machine).pop(item)
			a.get(machine).update(b.get(machine))
		else:
				a.update(b)
	return
    


def make_dictionary(lst):
    a = {}
    for item in lst:
        tmp = []
        tmp.append(item[1])
        aux = {item[2] : {item[0] : tmp}}
        merge_dict (a, aux)
    return a


def find_pid(txt):
	dic = {}
	try:
		pattern = r"[0-9], (.+?)\[.*?:(.+?)\]@(.+?),"
		found = re.findall(pattern, txt)
		pid = [list(x) for x in found]
		dic = make_dictionary(pid)

	except AttributeError:
			found = '' # apply your error handling
	return dic


def find_line(txt):
	try:
		pattern = r"Verification\s+=\s+(\w*)"
		found = re.findall(pattern, txt)
		res = found[0]

	except AttributeError:
			res = '' # apply your error handling
	return res


def dmtcp_running():
	cmd = "dmtcp_command -s"
	txt = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf-8')
	pattern=r"NUM_PEERS=(.*)\s*RUNNING=(.*)"
	found = re.findall(pattern, txt)
	# print (found)
	try:
		t = found[0][0]
	except IndexError:
		return 0 # kill dmtcp with 'dmtcp_command -q'

	if int(found[0][0]) <= 3:
		return 0 # kill dmtcp with 'dmtcp_command -q'
	elif found[0][1] == "yes":
		return 1 # running normal
	# else:
#             return 2 # program is not running (checkpointing, restarting or finished)
	elif found[0][1] == "no" and int(found[0][0]) > 0:
		return 2 # program is not running (checkpointing, restarting or finished))
	else:
		return 3 # only dmtcp_cordinator running

def checkpoint(interval, std_file_name):

	# check if aplication is running

    stdout_file = EXECUTION_LOGS_PATH + "/dmtcp_checkpoint_stdout.txt"
    stderr_file = EXECUTION_LOGS_PATH + "/dmtcp_checkpoint_stderr.txt"
    out = open(stdout_file,"w")
    err = open(stderr_file,"w")

    print("--waiting to checkpoint. Interval set to: " + str(interval) + " seconds")
    time.sleep(interval)
    times_ckpt = []
    res = "ERROR"
    while dmtcp_running() == 1:  
              
        if dmtcp_running() == 1:

            # realize checkpoint
            print("--Checkpointing...")

            start = time.time()
            print("--- Started -> epoch: "+str(start) )

            cmd = "dmtcp_command -c"
            subprocess.run(cmd, shell=True, stdout=out, stderr=err)
            #waits for checkpoint to finish
            while (dmtcp_running() == 2):
                time.sleep(.5)

            end = time.time()            
            t_ckpt = end - start
            print("--- Ended -> epoch: "+str(end) )
            print("--- Ckp Time -> "+str(t_ckpt) )

            times_ckpt.append(t_ckpt)
        else:
            res = "--- Program ended"

        # ckp_size
        if CKP_STOP == 1:
            print("--- CKP_STOP = 1; Exiting checkpoint...")
            break
        
        print("--waiting to checkpoint. Interval set to: " + str(interval) + " seconds")
        time.sleep(interval)
        #return "checkpointing: "+ prog+ " took: " + end + "seconds\n"

    subprocess.run("dmtcp_command -q", shell=True)

    out.close()
    err.close()
    return times_ckpt

def prepare_checkpoint_folder(machines,ckptdir):
    for pc in machines:
        ssh = 'ssh ' + pc + ' '

        print ("--Deleting content of checkpoint files folder in: " + pc)
        cmd = ssh + "'rm -rf "+ckptdir+"/*'"
        subprocess.run(cmd, shell=True)

        print ("--Creating checkpoint files folder in: " + pc)
        cmd = ssh + "'mkdir -p "+ckptdir+"'"
        subprocess.run(cmd, shell=True)

def clear_enviroment(machines):
    cmd_cache = 'echo 3 | sudo tee /proc/sys/vm/drop_caches'
    cmd_erase_dmtcp_logs = 'rm -Rf '+  DMTCP_ORIGINAL_LOGS_PATH+'dmtcp-*'

    # local machine
    ## cache
    for pc in machines:
        ssh = 'ssh ' + pc + ' '
        
        print ("--Clear cache in: " + pc)
        cmd = ssh + "'"+cmd_cache + "'"
        subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        print ("--Deleting dmtcp original logs in: " + pc)
        cmd = ssh + "'"+cmd_erase_dmtcp_logs + "'"
        subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        
def prepare_environment(machines):
    pc = machines[0]

    ssh = 'ssh ' + pc + ' '

    print ("--Deleting content of results folder in: " + pc)
    cmd = ssh + "'rm -rf "+RESULTS_PATH+"/*'"
    subprocess.run(cmd, shell=True)

    print ("--Creating results folder in: " + pc)
    cmd = ssh + "'mkdir -p "+RESULTS_PATH+"'"
    subprocess.run(cmd, shell=True)

    print ("--Creating logs folder in: " + pc)
    cmd = ssh + "'mkdir -p "+LOGS_PATH+"'"
    subprocess.run(cmd, shell=True)

def prepare_execution_folder(machines):
    pc = machines[0]
    
    ssh = 'ssh ' + pc + ' '
    
    print ("--Creating execution logs folder in: " + pc)
    cmd = ssh + "'mkdir -p "+EXECUTION_LOGS_PATH+"'"
    subprocess.run(cmd, shell=True)
    

def start_coordinator(ckptdir, std_file_name):
    cmd = "dmtcp_coordinator --ckptdir " + ckptdir + " "

    stdout_file = EXECUTION_LOGS_PATH + "/dmtcp_coordinator_stdout.txt"
    stderr_file = EXECUTION_LOGS_PATH + "/dmtcp_coordinator_stderr.txt"

    with open(stdout_file,"w") as out, open(stderr_file,"w") as err:
        coord = subprocess.Popen(cmd, shell=True, stdout=out, stderr=err)

    time.sleep(1)


def execute_mpi_application_and_checkpoint(host, ckptdir, n_vcpu, bin_path, prog, interval, std_file_name):
    start_t = time.time()

    mpi = execute_mpi_application_with_dmtcp(host, ckptdir, n_vcpu, bin_path, prog, std_file_name)
    times_ckpt = execute_checkpoint(interval, std_file_name)

    end_t = time.time()
    total_t = end_t - start_t
    
    return total_t, times_ckpt

def execute_checkpoint(interval, std_file_name):
    times_ckpt = ""
    times_ckpt = checkpoint(interval, std_file_name)
    return times_ckpt

def execute_mpi_application_with_dmtcp(host, ckptdir, n_vcpu, bin_path, prog, std_file_name):
    cmd = "dmtcp_launch -h " + host + " -p 7779 --ckptdir " + ckptdir + " mpirun -n " + str(n_vcpu) + " -hostfile ~/hostname -ppn " + str(PPN) + " " + bin_path + prog
    
    stdout_file = EXECUTION_LOGS_PATH + "/mpi_dmtcp_stdout.txt"
    stderr_file = EXECUTION_LOGS_PATH + "/mpi_dmtcp_stderr.txt"

    with open(stdout_file,"w") as out, open(stderr_file,"w") as err:
        mpi = subprocess.Popen(cmd, shell=True, stdout=out, stderr=err)

    return mpi

def execute_mpi_application(host, n_vcpu, bin_path, prog, std_file_name):
    cmd = "mpirun -n " + str(n_vcpu) + " -hostfile ~/hostname -ppn " + str(PPN) + " " + bin_path + prog
    print("MPI : " + cmd)

    stdout_file = EXECUTION_LOGS_PATH + "/mpi_dmtcp_stdout.txt"
    stderr_file = EXECUTION_LOGS_PATH + "/mpi_dmtcp_stderr.txt"
    
    with open(stdout_file,"w") as out, open(stderr_file,"w") as err:
        mpi = subprocess.run(cmd, shell=True, stdout=out, stderr=err)

    return mpi

    

def write_header_file(filename):
    msg_header = "Benchmark TestNumber ExecutionTime NPBTime CheckpointsTotal CheckpointMean CheckpointTimes\n"
    fl = open(filename, "w")
    fl.write(msg_header)
    fl.close()

def write_header_file_restart(filename):
    msg_header = "Benchmark TestNumber RestartTime\n"
    fl = open(filename, "w")
    fl.write(msg_header)
    fl.close()
    
def save_results_restart(filename, prog, test_number, restart_time):
    msg_table = prog +  " " + str(test_number) +  " " + str(restart_time) +  "\n"
    
    fl = open(filename, "a")
    fl.write(msg_table)
    fl.close()

def save_results_restart_mean(filename, prog, restart_mean, restart_dev ):
    msg_table = prog +  " Mean: " + str(restart_mean) +  " Std:" + str(restart_dev) +  "\n"
    
    fl = open(filename, "a")
    fl.write(msg_table)
    fl.close()
    
    

def save_results(filename, prog, test_number, total_time, checkpoints_times):    
    mpi_file = EXECUTION_LOGS_PATH + "/mpi_dmtcp_stdout.txt"
    
    checkpoints_total = len(checkpoints_times)
     
    if(checkpoints_total == 0):
        checkpoints_times = "0"
        checkpoint_mean = 0
        checkpoint_sum = 0
    else:
        checkpoint_mean = statistics.mean(checkpoints_times)  
        checkpoint_sum = sum(checkpoints_times)      
        checkpoints_times =  ';'.join(str(i) for i in checkpoints_times)
    
    npb_time = npb.get_npb_time(mpi_file)
            
    msg_table = prog +  " " + str(test_number) +  " " + str(total_time) + " " + str(npb_time) + " " + str(checkpoints_total) +  " " +  str(checkpoint_mean) + " " + str(checkpoints_times) +  "\n"

    fl = open(filename, "a")
    fl.write(msg_table)

    fl.close()

def copy_original_dmtcp_logs(machines):
    # local machine
    src = glob.glob(DMTCP_ORIGINAL_LOGS_PATH+'dmtcp-*')[0]
    dst = EXECUTION_LOGS_PATH
    distutils.dir_util.copy_tree(src, dst)
    
    # other machines
    itermachines = iter(machines)
    next(itermachines)
    for pc in itermachines:
        cmd = 'scp -r ' + pc + ":" + DMTCP_ORIGINAL_LOGS_PATH + 'dmtcp-*' + " " + dst
        subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

def ckp_size(execution_name, origin, destination):
    cmd = "ls -la "+ origin + " > " + destination + "/cpk_files_sizes_ls_la.txt"
    #print("cmd: " + cmd)
    subprocess.run(cmd, shell=True) 

    cmd = "du -b "+ origin + " > " + destination + "/cpk_dir_size_du_b.txt"
    #print("cmd: " + cmd)
    subprocess.run(cmd, shell=True) 

def ckp_size_all_machines(machines, execution_name, path_origin, path_destination):  
    
    # save results in ebs
    num_node = 0
    for pc in machines:
        node_name = "node"+ str(num_node)

        cmd_ls = "ls -l "+ path_origin + " > " + path_destination + "/" +  node_name + "_"+ pc + "_ckp-sizes-ls.txt"
        cmd = 'ssh  ' + pc + " '" +cmd_ls + "'"
        subprocess.run(cmd, shell=True)
        
        num_node += 1
        
        cmd_du= "du -b "+ path_origin + " > " + path_destination + "/" +  node_name + "_"+ pc + "_ckp-sizes-du.txt"
        cmd = 'ssh  ' + pc + " '" +cmd_du + "'"
        subprocess.run(cmd, shell=True)
        

        
          
def finish_coordinator():
    subprocess.run("dmtcp_command -q", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

 
def run_without_checkpoint(total_tests, machines, host, n_vcpu, filename, bin_path, files):
    global EXECUTION_LOGS_PATH
    prog = ""

    prepare_environment(machines)
    write_header_file(filename)

    for prog in files:
        for i in range (total_tests):
            print ("Benchmarking Without Checkpoint " + str(i+1) + "/" + str(total_tests) + "  of:" + prog)
            execution_name = prog + "_" + str(i+1)
            EXECUTION_LOGS_PATH = LOGS_PATH + execution_name + "/"
            
            print("Cleaning enviroment in all machines")
            clear_enviroment(machines)
            
            print ("Preparing execution folder ")
            prepare_execution_folder(machines)

            print ("Executing MPI aplication")
            start_t = time.time()
            
            execute_mpi_application(host, n_vcpu, bin_path, prog, execution_name)
            
            end_t = time.time()
            total_t = end_t - start_t
            
            times_ckpt = []
            print ("Writing tests results to a file: "+ filename)
            save_results(filename, prog, i+1, total_t, times_ckpt)
            
            print("\n\n")
 
def run_with_checkpoint(total_tests, machines, host, n_vcpu, interval, filename, ckptdir, bin_path, files):
    global EXECUTION_LOGS_PATH
    prog = ""

    prepare_environment(machines)
    write_header_file(filename)


    for prog in files:
        for i in range (total_tests):
            print ("Benchmarking with checkpoint " + str(i+1) + "/" + str(total_tests) + "  of:" + prog)
            execution_name = prog + "_" + str(i+1)
            EXECUTION_LOGS_PATH = LOGS_PATH + execution_name + "/"
            
            print("Cleaning enviroment in all machines")
            clear_enviroment(machines)
            
            print ("Preparing execution folder ")
            prepare_execution_folder(machines)
            
            print ("Preparing checkpoint folder in all machines")
            prepare_checkpoint_folder(machines,ckptdir)

            print ("\n\nStarting coordinator")
            start_coordinator(ckptdir, execution_name)

            print ("Executing MPI aplication and checkpoint")
            total_t, times_ckpt = execute_mpi_application_and_checkpoint(host, ckptdir, n_vcpu, bin_path, prog, interval, execution_name)
        
            print ("Writing tests results to a file: " + filename)        
            save_results(filename, prog, i+1, total_t, times_ckpt)
    
            #print("Saving original dmtcp logs")
            #copy_original_dmtcp_logs(machines)
            
            print("Finishing coordinator")
            finish_coordinator()
            
            # print("\nCleaning enviroment in all machines")
            #clear_enviroment(machines)
            
            print("Saving ckp size files")
            ckp_size_all_machines(machines, execution_name, ckptdir, EXECUTION_LOGS_PATH)
            
            

            print ("Process has ended\n\n")


def checkpoint_restart(ckptdir):
    # for each host
    # dmtcp_restart --join-coordinator -p 7779  -h ip-172-31-69-199 ckpt*.dmtcp
    cmd = "cd " + ckptdir + "; ./dmtcp_restart_script.sh &"

    stdout_file = EXECUTION_LOGS_PATH + "/dmtcp_restart_stdout.txt"
    stderr_file = EXECUTION_LOGS_PATH + "/dmtcp_restart_stderr.txt"
    
    with open(stdout_file,"w") as out, open(stderr_file,"w") as err:
        mpi = subprocess.run(cmd, shell=True, stdout=out, stderr=err)

    return mpi

def run_checkpoint_restart(total_tests, machines, host, filename, ckptdir, npb_name):
    global EXECUTION_LOGS_PATH

    prepare_environment(machines)
    write_header_file_restart(filename)
    
    restart_times = []
    
    
    for i in range (total_tests):
        print()
        print("------------")
        print ("Benchmarking with restart " + str(i+1) + "/" + str(total_tests) + "  of:" + npb_name)
        print("Filename: "  +filename)
        
        
        execution_name = npb_name + "_" + str(i+1)
        EXECUTION_LOGS_PATH = LOGS_PATH + execution_name + "/"
        
        print("Cleaning enviroment in all machines")
        clear_enviroment(machines)

        print ("Preparing execution folder ")
        prepare_execution_folder(machines)

        print ("\n\nStarting coordinator")
        start_coordinator(ckptdir, ckptdir)

        print ("\n\nRestarting...")
        start = time.time()

        checkpoint_restart(ckptdir)

        # while (dmtcp_running() != 2): #  [('7', 'no')]
        #     # print("dmtcp_running(): " + str(dmtcp_running()))
        #     time.sleep(.5)
        while True:
        
            run = dmtcp_running()
            # print(time.strftime("[%Y/%m/%d %H:%M:%S]"), "Running Status: ", run, )
        
            if (run == 1): #  [('7', 'no')]
                # print("dmtcp_running(): " + str(dmtcp_running()))
                break
            time.sleep(.5)
    

        end = time.time()   
   
        t_restart = end - start
        print("Restart Time: " + str(t_restart))
        restart_times.append(t_restart)
    
        print ("Writing tests results to a file: " + filename)        
        save_results_restart(filename, npb_name, i+1, t_restart)
    
        print("Finishing DMTCP")
        subprocess.run("dmtcp_command -q", shell=True)
        time.sleep(1)
        
        print("Finishing coordinator")
        finish_coordinator()
        time.sleep(1)
    
    restart_mean = statistics.mean(restart_times)  
    restart_dev = statistics.pstdev(restart_times)
    save_results_restart_mean(filename, npb_name, restart_mean, restart_dev)
    print("NPB: " + npb_name )
    print("Restart times: " + str(restart_times))
    print("restart_mean: "+ str(restart_mean))
    print("restart_dev: "+ str(restart_dev))

        


RESULTS_PATH = ""
LOGS_PATH = ""
EXECUTION_LOGS_PATH = "" 
DMTCP_ORIGINAL_LOGS_PATH =  "" 
HOSTNAME_FILE = "/home/ubuntu/hostname"
NPB_BIN_PATH = "/home/ubuntu/NPB3.3.1/NPB3.3-MPI/bin/"
PPN = 4
CKP_STOP=0

def ckp_path(storage_type):
    return {
        'local': "/home/ubuntu/checkpoint_files",
        'ssd' : '/home/ubuntu/ssd/checkpoint_files',
        'ebs' : '/home/ubuntu/ebs/checkpoint_files',
        'efs' : '/home/ubuntu/efs/checkpoint_files',
        'fsx' : '/home/ubuntu/fsx/checkpoint_files',
        's3fs': '/home/ubuntu/s3fs/checkpoint_files'
    }.get(storage_type,  'Invalid storage')    

    
def main():
    global RESULTS_PATH
    global LOGS_PATH
    global EXECUTION_LOGS_PATH
    global DMTCP_ORIGINAL_LOGS_PATH
    global HOSTNAME_FILE
    global NPB_BIN_PATH
    global PPN
    global CKP_STOP
 
    
    if len(sys.argv) < 5 :
        print(sys.argv)
        print("Please, insert valid parameters")
        print("./dmtcp_bench_time_aws.py [ckp_yes|ckp_no|ckpt_stop|ckpt_restart] [vCPUS] [PPN] [totalTest] [NPB_app][ckp_storage_type] ")
        print("./dmtcp_bench_time_aws.py [ckp_yes|ckp_no|ckpt_stop|ckpt_restart] [vCPUS] [PPN] [totalTest] [npb_all|npb_select][interval][ebs|efs|fsx|s3sf]")
        print("./dmtcp_bench_time_aws.py [ckp_yes|ckp_no|ckpt_stop|ckpt_restart] [vCPUS] [PPN] [totalTest] [ckp_no] [denise][ebs|efs|fsx|s3sf]")
        print("./dmtcp_bench_time_aws.py ckp_yes 128 64 3  npb_all 60 efs")
        print("./dmtcp_bench_time_aws.py ckp_no 128 64 3  npb_select")
        print("./dmtcp_bench_time_aws.py ckp_restart 128 64 3 npb_select [restart_path]")
        exit (1)
    
    do_ckp = sys.argv[1]
    n_vcpu = int(sys.argv[2])
    PPN = int(sys.argv[3]) #Process per Node
    total_tests = int(sys.argv[4])
    npb_app = sys.argv[5]
    
    
    storage_type= "efs"
    interval = 60
    
    if len(sys.argv) == 8 :
        interval = int(sys.argv[6])
        storage_type = sys.argv[7]
    
    # machines and host 
    #file = open("", "r")
    file = open(HOSTNAME_FILE, "r")
    machines = [line.rstrip('\n') for line in file]
    host = socket.gethostname()
        
    # results
    ts = time.time()
    name = storage_type + "_" +str(n_vcpu) + "cores_"+str(ts)
    RESULTS_PATH = "/home/ubuntu/efs_results/results/"+  name + "/"
    results_filename = RESULTS_PATH + "results.txt"
    
    # logs
    LOGS_PATH = RESULTS_PATH + "logs/"
    EXECUTION_LOGS_PATH = LOGS_PATH # global variable (will be changed)

    # npb 
    files = []
    if npb_app == "npb_all":
        pattern = r"^[a-z]{2}\.D\."+str(n_vcpu)
        files = [i for i in os.listdir(NPB_BIN_PATH) if re.match(pattern, i)]
    if npb_app == "npb_select":
        #files = ["ep.D."+str(n_vcpu)]
        files = ["lu.D."+str(n_vcpu)]#, "mg.B."+str(n_vcpu)]
    
    # prints
    print("Started -> Local time: " + time.ctime(ts) +" , epoch: " + str(ts) )
    print("Machines: " + str(machines))
    print("VCPU: " +str(n_vcpu))
    print("PPN: " + str(PPN))
    print("Total tests: " + str(total_tests))
    print("Do checkpoint: " + do_ckp)
    print("NPB: " + str(files))
    print("Results path: " + RESULTS_PATH)
    
    if do_ckp == "ckp_no":
        print()
        run_without_checkpoint(total_tests, machines, host, n_vcpu, results_filename, NPB_BIN_PATH, files)
    
    if do_ckp == "ckp_stop":
        CKP_STOP=1 # stop after first checkpoint
        do_ckp = "ckp_yes"
      
    if do_ckp == "ckp_yes":
        ckptdir = ckp_path(storage_type)
        print("Chekpoint Interval: " + str(interval))
        print("Checkpoint Interval:" + str(interval))
        print("Checkpoint directory: " + str    (ckptdir))
        print()
        run_with_checkpoint(total_tests, machines, host, n_vcpu, interval, results_filename, ckptdir, NPB_BIN_PATH, files)
    
    if do_ckp == "ckp_restart":
        ckptdir = sys.argv[6]
        print("Checkpoint directory: " + str    (ckptdir))
        npb_name = files[0] #just one
        run_checkpoint_restart(total_tests, machines, host, results_filename, ckptdir, npb_name)

        
        
    ts=time.time()
    print("Finished -> Local time:" + time.ctime(ts) +  " , epoch: "+str(ts) )
    
if __name__ == '__main__':
    main()
