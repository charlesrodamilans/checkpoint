# Mmap tip
# https://stackoverflow.com/questions/4940032/how-to-search-for-a-string-in-text-files
import statistics 
import collections

def search_line(filename, search):
    with open(filename, "r") as file:
        for l in file:
            l = l.rstrip('\n')
            if l.find(search) != -1:
                return l
    
def get_npb_time(filename):
    line = search_line(filename, "Time in seconds")
    time = 0
    try :
        time = line.split()[-1:][0]
    except:
        print("Error in npb.get_npb_time(). ")
        print("Error in file: \n" + str(filename))
    return float(time)
        
def get_npb_name(filename):
    line = search_line(filename, "NAS Parallel Benchmarks")
    return line.split()[-2:][0]

def bechs_mean_dev(filenames_list):
    # inicialize
    npb_bench_values = collections.defaultdict(list)
    for f in filenames_list:
        time = get_npb_time(f)
        name = get_npb_name(f)
        npb_bench_values[name].append(time)
  
    npb_bench_mean = collections.defaultdict(list)
    npb_bench_dev = collections.defaultdict(list)
    for name in npb_bench_values.keys():
        npb_bench_mean[name] = statistics.mean(npb_bench_values[name])
        npb_bench_dev[name] = statistics.pstdev(npb_bench_values[name])
        
    return  npb_bench_mean, npb_bench_dev

def print_results_horizontal(dic):
    for key in sorted(dic.keys()):
        print(key, end = '\t')
    print()

    for key in sorted(dic.keys()):
        print(dic[key], end = '\t')
    print()
    