import subprocess

#prepare_execution_folder
def host_execution(machines, execution_logs_path):
    pc = machines[0]
    
    ssh = 'ssh ' + pc + ' '
    
    print ("--Creating execution logs folder in: " + pc)
    cmd = ssh + "'mkdir -p "+execution_logs_path+"'"
    subprocess.run(cmd, shell=True)

def host_enviroment(machine, results_path, logs_path):
    pc = machine

    ssh = 'ssh ' + pc + ' '

    print ("--Deleting content of results folder in: " + pc)
    cmd = ssh + "'rm -rf "+results_path+"/*'"
    subprocess.run(cmd, shell=True)

    print ("--Creating results folder in: " + pc)
    cmd = ssh + "'mkdir -p "+results_path+"'"
    subprocess.run(cmd, shell=True)

    print ("--Creating logs folder in: " + pc)
    cmd = ssh + "'mkdir -p "+logs_path+"'"
    subprocess.run(cmd, shell=True)

def checkpoint_folder(machines,ckptdir):
    for pc in machines:
        ssh = 'ssh ' + pc + ' '

        print ("--Creating checkpint folder in: " + pc)
        cmd = ssh + "'rm -rf "+ckptdir+"/*'"
        subprocess.run(cmd, shell=True)

        cmd = ssh + "'mkdir -p "+ckptdir+"'"
        subprocess.run(cmd, shell=True)
        
def clear_cache(machines):
    # clear cache
    cmd_cache = 'echo 3 | sudo tee /proc/sys/vm/drop_caches'

    for pc in machines:
        ssh = 'ssh ' + pc + ' '
        
        print ("--Clear cache in:  " + pc)
        cmd = ssh + "'"+cmd_cache + "'"
        subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)