import subprocess
import time
from . import dmtcp 
from . import prepare
from . import results
from . import npb
    
def run_mpi(n_vcpu, ppn, bin_path, prog, execution_logs_path, ckp_command=''):
    stdout_file = execution_logs_path + "/mpi_stdout.txt"
    stderr_file = execution_logs_path + "/mpi_stderr.txt"
    
    cmd = ckp_command + " mpirun -n " + str(n_vcpu) + " -hostfile ~/hostname -ppn " + str(ppn) + " " + bin_path + prog
    print ("MPI: " + cmd)
    with open(stdout_file,"w") as out, open(stderr_file,"w") as err:
        result_mpi = subprocess.run(cmd, shell=True, stdout=out, stderr=err)
    
    return result_mpi


def run_mpi_and_checkpoint(host, ckptdir, n_vcpu, ppn, bin_path, prog, interval, execution_logs_path):    
    print("run_mpi_and_checkpoint 1 ")
    ckp_command = "dmtcp_launch -h " + host + " -p 7779 --ckptdir " + ckptdir
    run_mpi(n_vcpu, ppn, bin_path, prog, execution_logs_path, ckp_command)
    print("run_mpi_and_checkpoint 2")
    times_ckpt = dmtcp.checkpoint(interval, execution_logs_path)
    return times_ckpt
    
def run_experiments_with_checkpoint(total_tests, machines, n_vcpu, ppn, bin_path, benchmarks_files, logs_path, results_filename, 
                                    results_path, host, checkpoint_interval, checkpoint_path):
    prog = ""

    prepare.host_enviroment(host, results_path, logs_path)
    results.write_header_file(results_filename)


    for prog in benchmarks_files:
        for i in range (total_tests):
            print ("Benchmarking with checkpoint " + str(i+1) + "/" + str(total_tests) + "  of:" + prog)
            execution_name = prog + "_" + str(i+1)
            execution_logs_path = logs_path + execution_name + "/"
                        
            print("Cleaning cache in all machines")
            prepare.clear_cache(machines)
            
            print ("Preparing execution ")
            prepare.host_execution(machines, execution_logs_path)
            
            print ("Preparing checkpoint folder in all machines")
            prepare.checkpoint_folder(machines,checkpoint_path)

            print ("Starting coordinator")
            dmtcp.start_coordinator(checkpoint_path, execution_logs_path)

            print ("Executing MPI aplication and checkpoint")
            start_t = time.time()
            times_ckpt = run_mpi_and_checkpoint(host, checkpoint_path, n_vcpu, ppn, bin_path, prog, checkpoint_interval, execution_logs_path)
            end_t = time.time()
            total_t = end_t - start_t
            
            print ("Writing tests results to a file: " + results_filename) 
            mpi_file = execution_logs_path + "/mpi_stdout.txt"       
            npb_time = npb.get_npb_time(mpi_file)
            results.write_results_file(results_filename, npb_time, prog, i+1, total_t, times_ckpt)
            
            print("Finishing coordinator")
            dmtcp.finish_coordinator()
            
            # print("Cleaning enviroment in all machines")
 #            clear_enviroment(machines)

            print ("Process has ended\n\n")

def run_experiments_without_checkpoint(total_tests, machines, host, n_vcpu, ppn, interval, results_filename, \
                                        bin_path, files,results_path, logs_path):

    prog = ""

    prepare.host_enviroment(machines[0], results_path, logs_path)
    results.write_header_file(results_filename)

    for prog in files:
        for i in range (total_tests):
            print ("Benchmarking Without Checkpoint " + str(i+1) + "/" + str(total_tests) + "  of:" + prog)
            execution_name = prog + "_" + str(i+1)
            execution_logs_path = logs_path + execution_name + "/"
            
            print("Cleaning cache in all machines")
            prepare.clear_cache(machines)
            
            print ("Preparing execution ")
            prepare.host_execution(machines, execution_logs_path)

            print ("Executing MPI aplication")
            start_t = time.time()
            
            run_mpi(n_vcpu, ppn, bin_path, prog, execution_logs_path)
            
            end_t = time.time()
            total_t = end_t - start_t
            
            times_ckpt = []
            print ("Writing tests results to a file: "+ results_filename)
            mpi_file = execution_logs_path + "/mpi_stdout.txt"       
            npb_time = npb.get_npb_time(mpi_file)
            results.write_results_file(results_filename, npb_time, prog, i+1, total_t, times_ckpt)