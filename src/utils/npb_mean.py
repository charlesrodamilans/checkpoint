import npb
import sys    
import os   


    
def main():
    data_paths = []
    
    # select specific results (with input) or all results  
    if (len(sys.argv) <  1):
        print('Please insert file with npb file names')
    
    filename = sys.argv[1]
    filenames_list = [line.rstrip('\n') for line in open(filename)]
    
    npb_bench_mean, npb_bench_dev = npb.bechs_mean_dev(filenames_list)
    npb_bench_mean, npb_bench_dev = npb.bechs_mean_dev(filenames_list)
    
    
    npb.print_results_horizontal(npb_bench_mean)
    npb.print_results_horizontal(npb_bench_dev)


if __name__ == '__main__':
    main()
