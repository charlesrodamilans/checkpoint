#!/usr/local/bin/python3
import sys
import numpy as np

def ckptime_mean(datapath):
    types = ['U50', 'i4', 'f8','f8', 'i4', 'f8', 'U5000']
    # Benchmark TestNumber ExecutionTime NPBTime CheckpointsTotal CheckpointMean CheckpointTimes
    # is.D.128 1 60.11362648010254 19.76 0 0 0
    data = np.genfromtxt(datapath, dtype=types,  delimiter=' ', names=True)
    print(data)
    print()
    bench_values = {}
    for i in range(0, len(data)):
        bench_values[data[i][0]] = []

    for i in range(0, len(data)):
        aux = [float(i) for i in data[i][6].split(";")]
        bench_values[data[i][0]].extend(aux)

    print("Benchmark\tCkptTimesMean\tCkptTimesStd")
    for k in sorted(bench_values.keys()):
        # print(k, np.mean(bench_values[k]),  np.std(bench_values[k]))
        print("%s\t%f\t%f" %(k, np.mean(bench_values[k]),  np.std(bench_values[k])))
        
    



def main():
    data_paths = []

    # select log directory
    if (len(sys.argv) <  1):
        print('Please insert file')
        print('./ckptimes.py results.txt')

    datapath = sys.argv[1]
    print(datapath)
    ckptime_mean(datapath)

if __name__ == '__main__':
    main()
