#!/bin/python

import glob
import os
import sys
import statistics 

# in bytes
def get_du_size(filename):
    with open(filename, "r") as file:
        line = file.readline()
        return int(line.split()[0])
    
    
# return in KiB
def get_cpk_total_size_one_node(filename):
    with open(filename, "r") as file:
        line = file.readline()
        return int(line.split()[1])

# just return filesnames
def list_filenames_in_directory(dir_path, word):
    path = dir_path+'/*'+ word + "*"
    return [os.path.basename(x) for x in glob.glob(path)]    

# list files with absolute path
def list_files_in_directory(dir_path, word):
    path = dir_path+'/*'+ word + "*"
    return glob.glob(path)
            
# return in KiB
def get_cpk_total_sizes_all_nodes(dir_path):
    files_path = list_files_in_directory(dir_path, "ckp-sizes-ls.txt") # return full path
    dic = {}
    for file_path in files_path:
        size = get_cpk_total_size_one_node(file_path)
        
        filename = os.path.basename(file_path)
        node_name = filename.split("_")[0]
        
        dic[node_name] = size
                    
    return dic
   
def get_cpk_total_sizes_all_benchs(dir_path):   
    dir_paths = glob.glob(dir_path+'/*')
    dic = {}
    for dp in dir_paths:
        sizes = get_cpk_total_sizes_all_nodes(dp)
        bench_name = os.path.basename(os.path.normpath(dp))
        dic[bench_name] = sizes
    return dic

def average_cpk_size_one_node(ckp_sizes):
    return [statistics.mean(ckp_sizes.values()), statistics.pstdev(ckp_sizes.values())]
    
# print in KiBytes
def print_ckp_average(bench_ckp_sizes):
    print("Bench_name ckp_size_mean(KiB) ckp_size_stddev(KiB)")
    for bench, ckp_sizes in bench_ckp_sizes.items():
        mean = statistics.mean(ckp_sizes.values())
        std = statistics.pstdev(ckp_sizes.values())
        print(bench + " " + str(mean) + " " + str(std) )

def print_ckp_average_node_type(bench_ckp_sizes):
    print("Bench_name node0_ckp_size(KiB) others_nodes(KiB) others_nodes_stddev(KiB) other_nodes_sizes(KiB)")
    for bench, ckp_sizes in bench_ckp_sizes.items():
        node0_size = ckp_sizes['node0']
       
        others_sizes =dict(ckp_sizes)
        del others_sizes['node0']
        
       
        
        if(len(others_sizes.values()) > 0):
            others_sizes_mean = statistics.mean(others_sizes.values())
            others_sizes_std = statistics.pstdev(others_sizes.values())
            others_sizes = ';'.join(map(str, list(others_sizes.values())))
        else:
            others_sizes_mean = 0
            others_sizes_std = 0
            others_sizes=0
        
        print(bench + " " + str(node0_size) + " " + str(others_sizes_mean) + " " + str(others_sizes_std) \
            + " " + str(others_sizes) )
          
         
def main():
    data_paths = []
    
    # select log directory
    if (len(sys.argv) <  1):
        print('Please insert log path')
        print('./ckp.py ~/local_64cores_1569253962.490715/logs/')
    
    dir_path = sys.argv[1]
    results = get_cpk_total_sizes_all_benchs(dir_path)    
    # print(results)
    print_ckp_average_node_type(results)

if __name__ == '__main__':
    main()
