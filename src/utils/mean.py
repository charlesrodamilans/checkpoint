#!/bin/python3
import sys
import os
import numpy as np

def main():
    data_paths = []
    
    # select specific results (with input) or all results  
    if (len(sys.argv) ==  2):
        data_paths.append(sys.argv[1])
        print('Using Specific File ')
    else :
       directories = next(os.walk('.'))[1]
       for d in directories:
           data_paths.append(d+"/results.txt")
       print('Using all directories. ')
    
    # print all paths
    # print(data_paths)
#     print()


    for data_path in data_paths:
        
        # load data
        types = ['U50', 'i4', 'f8','i4']
        data = np.genfromtxt(data_path, dtype=types,  delimiter=' ', names=True)

        # calcule median
        dic = {}
        for i in range(0, len(data)):
            dic[data[i][0]] = 0

        for i in range(0, len(data)):
            dic[data[i][0]] += data[i][2]

        for key in dic.keys():
            dic[key] /= 3

        # print results
        print()
        print("Median for " + data_path )

        for key in dic.keys():
            print(key, end = '\t')
        print()

        for key in dic.keys():
            print(dic[key], end = '\t')
        print()
    
if __name__ == '__main__':
    main()

