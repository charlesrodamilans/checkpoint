import time
import subprocess
import re

def dmtcp_running():
	cmd = "dmtcp_command -s"
	txt = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf-8')
	pattern=r"NUM_PEERS=(.*)\s*RUNNING=(.*)"
	found = re.findall(pattern, txt)
	# print (found)
	try:
		t = found[0][0]
	except IndexError:
		return 0 # kill dmtcp with 'dmtcp_command -q'

	if int(found[0][0]) <= 3:
			return 0 # kill dmtcp with 'dmtcp_command -q'
	elif found[0][1] == "yes":
			return 1 # running normal
	else:
			return 2 # program is not running (checkpointing, restarting or finished)

# dmtcp.checkpoint: adicionar lancar excecão quando programar finalizar por erro "--Program ended"
def checkpoint(interval, execution_logs_path):

	# check if aplication is running

    stdout_file = execution_logs_path + "/dmtcp_checkpoint_stdout.txt"
    stderr_file = execution_logs_path + "/dmtcp_checkpoint_stderr.txt"
    out = open(stdout_file,"w")
    err = open(stderr_file,"w")

    print("--waiting to checkpoint. Interval set to: " + str(interval) + " seconds")
    time.sleep(interval)
    times_ckpt = []
    res = "ERROR"
    while dmtcp_running() == 1:  
              
        if dmtcp_running() == 1:

            # realize checkpoint
            print("--Checkpointing...")

            start = time.time()

            cmd = "dmtcp_command -c"
            subprocess.run(cmd, shell=True, stdout=out, stderr=err)
            #waits for checkpoint to finish
            while (dmtcp_running() == 2):
                time.sleep(.5)

            end = time.time()

            t_ckpt = end - start

            times_ckpt.append(t_ckpt)
        else:
            res = "--Program ended"

        print("--waiting to checkpoint. Interval set to: " + str(interval) + " seconds")
        time.sleep(interval)
        #return "checkpointing: "+ prog+ " took: " + end + "seconds\n"

    subprocess.run("dmtcp_command -q", shell=True)

    out.close()
    err.close()
    return times_ckpt

def start_coordinator(ckptdir, execution_logs_path):
    stdout_file = execution_logs_path + "/dmtcp_coordinator_stdout.txt"
    stderr_file = execution_logs_path + "/dmtcp_coordinator_stderr.txt"
    
    cmd = "dmtcp_coordinator --ckptdir " + ckptdir + " "

    stdout_file = execution_logs_path + "/dmtcp_coordinator_stdout.txt"
    stderr_file = execution_logs_path + "/dmtcp_coordinator_stderr.txt"

    with open(stdout_file,"w") as out, open(stderr_file,"w") as err:
        coord = subprocess.Popen(cmd, shell=True, stdout=out, stderr=err)

    time.sleep(1)

def finish_coordinator():
    subprocess.run("dmtcp_command -q", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
