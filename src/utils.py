def merge_dict(a, b):
	temp = []
	for machine in b:
		if machine in a:
			for process in b.get(machine):
				if process in a.get(machine):
					aux = []
					aux.extend(b.get(machine).get(process))
					aux.extend(a.get(machine).get(process))
					a.get(machine).update({process: aux})
					temp.append(process)
			for item in temp:
				b.get(machine).pop(item)
			a.get(machine).update(b.get(machine))
		else:
				a.update(b)
	return
    


def make_dictionary(lst):
    a = {}
    for item in lst:
        tmp = []
        tmp.append(item[1])
        aux = {item[2] : {item[0] : tmp}}
        merge_dict (a, aux)
    return a


def find_pid(txt):
	dic = {}
	try:
		pattern = r"[0-9], (.+?)\[.*?:(.+?)\]@(.+?),"
		found = re.findall(pattern, txt)
		pid = [list(x) for x in found]
		dic = make_dictionary(pid)

	except AttributeError:
			found = '' # apply your error handling
	return dic

def find_line(txt):
	try:
		pattern = r"Verification\s+=\s+(\w*)"
		found = re.findall(pattern, txt)
		res = found[0]

	except AttributeError:
			res = '' # apply your error handling
	return res