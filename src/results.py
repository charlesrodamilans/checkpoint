from . import npb
import statistics


def write_header_file(filename):
    msg_header = "Benchmark TestNumber ExecutionTime NPBTime CheckpointsTotal CheckpointMean CheckpointTimes\n"
    fl = open(filename, "w")
    fl.write(msg_header)
    fl.close()

def write_results_file(filename, npb_time, prog, test_number, total_time, checkpoints_times):    
    
    
    checkpoints_total = len(checkpoints_times)
     
    if(checkpoints_total == 0):
        checkpoints_times = "0"
        checkpoint_mean = 0
        checkpoint_sum = 0
    else:
        checkpoint_mean = statistics.mean(checkpoints_times)  
        checkpoint_sum = sum(checkpoints_times)      
        checkpoints_times =  ';'.join(str(i) for i in checkpoints_times)
                
    msg_table = prog +  " " + str(test_number) +  " " + str(total_time) + " " + str(npb_time) + " " + str(checkpoints_total) +  " " +  str(checkpoint_mean) + " " + str(checkpoints_times) +  "\n"

    fl = open(filename, "a")
    fl.write(msg_table)

    fl.close()