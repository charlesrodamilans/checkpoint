import unittest
from src import npb
import os

# cd checkpoint
# python3 -m unittest discover -s test/  -v

class TestNPB(unittest.TestCase):
   
    def test_get_npb_line(self):
        cur_path = os.path.abspath('.') 
        filename = cur_path + "/test/npb/bt.D.256.txt"
        
        result = npb.search_line(filename, "Time in seconds")
        expected = " Time in seconds =                   193.92"
        
        self.assertEqual(result, expected)

    def test_get_npb_time(self):
        cur_path = os.path.abspath('.') 
        filename = cur_path + "/test/npb/bt.D.256.txt"
        
        self.assertEqual(193.92, npb.get_npb_time(filename))

    def test_get_npb_time_all_files(self):
        test_path = os.path.abspath('.') + "/test/npb"
        filename = test_path + "/npb_times.txt"
        
        with open(filename, 'r') as file:   
            for line in file:
                v  = line.split()
                
                result = npb.get_npb_time(test_path+ "/" + v[0])
                expected = float(v[1])
                self.assertEqual(result, expected)
    
    def test_get_npb_name(self):
        cur_path = os.path.abspath('.') 
        filename = cur_path + "/test/npb/bt.D.256.txt"
        self.assertEqual("BT", npb.get_npb_name(filename))            
        
    # def test_not_find_line(self):
    #     pass

    # def bechs_mean(self):
    #     pass
        # npb_files.txt