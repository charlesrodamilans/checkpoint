import unittest
from src import ckp
import os

# cd checkpoint
# python3 -m unittest discover -s test/  -v


class TestCkp(unittest.TestCase):
   
    def test_get_ckp_du_size(self):
        cur_path = os.path.abspath('.') 
        filename = cur_path + "/test/ckp_size/individuals_files/cpk_dir_size_du_b.txt"
        
        result = ckp.get_du_size(filename)
        expected = 459794476
        
        self.assertEqual(result, expected)

    def test_ckp_get_ls_dir_size(self):
        cur_path = os.path.abspath('.') 
        filename = cur_path + "/test/ckp_size/individuals_files/cpk_files_sizes_ls_la.txt"
        
        result = ckp.get_cpk_total_size_one_node(filename) # KB
        expected = 449048
        
        self.assertEqual(result, expected)

    def test_ckp_list_filenames_in_directory(self):
        cur_path = os.path.abspath('.')
        dir_path = cur_path + "/test/ckp_size/files_in_directories/16cores_many_benchs/logs/bt.D.16_1"
        
        result = ckp.list_filenames_in_directory(dir_path, "ckp-sizes-ls.txt")

        expected =  ['node0_ip-172-31-12-33_ckp-sizes-ls.txt',
            'node1_ip-172-31-0-94_ckp-sizes-ls.txt',
            'node2_ip-172-31-4-201_ckp-sizes-ls.txt',
            'node3_ip-172-31-8-185_ckp-sizes-ls.txt'
        ]
        
        self.assertCountEqual(result, expected)
  

    def test_get_cpk_total_sizes_all_nodes(self):
        cur_path = os.path.abspath('.')
        dir_path = cur_path + "/test/ckp_size/files_in_directories/16cores_many_benchs/logs/bt.D.16_1"

        result = ckp.get_cpk_total_sizes_all_nodes(dir_path)

        #node, ckps_size
        expected = {
            'node0': 461308,
            'node1': 449144,
            'node2': 449048,
            'node3': 449612
        }

        self.assertCountEqual(result, expected)

    def test_average_cpk_size_one_node(self):
        cur_path = os.path.abspath('.')
        dir_path = cur_path + "/test/ckp_size/files_in_directories/16cores_many_benchs/logs/bt.D.16_1"

        sizes = ckp.get_cpk_total_sizes_all_nodes(dir_path)
        mean_result, stdev_result = ckp.average_cpk_size_one_node(sizes)
        
        mean_expected = 452278
        
        self.assertEqual(mean_result, mean_expected)
    
    def test_get_cpk_total_sizes_all_nodes_all_benchs(self):
        cur_path = os.path.abspath('.')
        dir_path = cur_path + "/test/ckp_size/files_in_directories/16cores_many_benchs/logs"

        results = ckp.get_cpk_total_sizes_all_benchs(dir_path)
        
        bt = {
            'node0': 461308,
            'node1': 449144,
            'node2': 449048,
            'node3': 449612
        }
        
        cg = {
            'node0': 1121228,
            'node1': 1133420,
            'node2': 958872,
            'node3': 1017812
        }
        
        expected = {'bt.D.16_1': bt, 'cg.D.16_1': cg}
        self.assertDictEqual(result, expected)
    
    def test_get_cpk_total_sizes_all_nodes_all_benchs(self):
        cur_path = os.path.abspath('.')
        dir_path = cur_path + "/test/ckp_size/files_in_directories/16cores_many_benchs/logs"

        result = ckp.get_cpk_total_sizes_all_benchs(dir_path)
    
#
# def average_cpk_sizes_all_benchs(ckp_bench):
#     pass
#
# def print_average_cpk_sizes_all_benchs(ckp_bench_average):
#     pass
    
    