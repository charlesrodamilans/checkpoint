

 NAS Parallel Benchmarks 3.3 -- MG Benchmark

 No input file. Using compiled defaults 
 Size: 1024x1024x1024  (class D)
 Iterations:   50
 Number of processes:    256

 Initialization time:           0.935 seconds

  iter    1
  iter    5
  iter   10
  iter   15
  iter   20
  iter   25
  iter   30
  iter   35
  iter   40
  iter   45
  iter   50

 Benchmark completed 
 VERIFICATION SUCCESSFUL 
 L2 Norm is  0.1583275060422E-09
 Error is    0.1142743095882E-10


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                    21.19
 Total processes =                      256
 Compiled procs  =                      256
 Mop/s total     =                146936.89
 Mop/s/process   =                   573.97
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              30 Jun 2019

 Compile options:
    MPIF77       = mpif90
    FLINK        = $(MPIF77)
    FMPI_LIB     = -L/usr/local/lib -lmpi
    FMPI_INC     = -I/usr/local/include
    FFLAGS       = -O
    FLINKFLAGS   = -O
    RAND         = randi8


 Please send feedbacks and/or the results of this run to:

 NPB Development Team 
 Internet: npb@nas.nasa.gov


