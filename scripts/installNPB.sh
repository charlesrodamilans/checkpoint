#!/bin/bash
sudo apt-get update &&
#sudo apt-get install -y wget make gcc libgfortran3 sysstat libibnetdisc-dev openmpi-bin libopenmpi-dev libhdf5-openmpi-dev gfortran build-essential git &&
sudo apt-get -y install  wget make gcc libgfortran3 sysstat libibnetdisc-dev mpich gcc g++ make python build-essential gfortran build-essential git 

wget https://www.nas.nasa.gov/assets/npb/NPB3.3.1.tar.gz &&
tar -zxf NPB3.3.1.tar.gz &&
cd NPB3.3.1/NPB3.3-MPI/ &&

cp config/make.def.template config/make.def &&
cat config/make.def.template | sed "s/MPIF77 = f77/MPIF77 = mpif90/g" | sed "s/MPICC = cc/MPICC = mpicc/g" > config/make.def &&
cp config/suite.def.template config/suite.def &&

np=4
for bench in bt cg ep ft is lu mg sp; do
    for size in A B C; do
        echo "$bench $size 1024" >> config/suite.def
        echo "$bench $size 512" >> config/suite.def
        echo "$bench $size 256" >> config/suite.def
        echo "$bench $size 128" >> config/suite.def
        echo "$bench $size 64" >> config/suite.def
        echo "$bench $size 16" >> config/suite.def
        echo "$bench $size 8" >> config/suite.def
        echo "$bench $size 4" >> config/suite.def
        echo "$bench $size 2" >> config/suite.def
    done
done

make suite



