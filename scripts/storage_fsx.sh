#!/bin/bash



function fsx_configure_and_reboot () {
	sudo apt-get install linux-image-4.4.0-131-generic
	sudo apt-get install -y linux-headers-4.4.0-131-generic
	sudo sed -i 's/GRUB_DEFAULT=.\+/GRUB\_DEFAULT="Advanced options for Ubuntu>Ubuntu, with Linux 4.4.0-131-generic"/' /etc/default/grub
	sudo update-grub
	sudo reboot &

}


function fsx_continue_after_reboot () {
	directory=$1
	file_system_dns_name=$2	
	# install
	wget https://downloads.whamcloud.com/public/lustre/lustre-2.10.6/ubuntu1604/client/lustre-client-modules-4.4.0-131-generic_2.10.6-1_amd64.deb
	wget https://downloads.whamcloud.com/public/lustre/lustre-2.10.6/ubuntu1604/client/lustre-utils_2.10.6-1_amd64.deb
	sudo apt-get install -y ./lustre-*_2.10.6*.deb

	# mount
	sleep 10
	sudo mkdir -p $directory
	echo ""
	echo "sudo mount -t lustre -o noatime,flock $file_system_dns_name@tcp:/fsx $directory"
	sudo mount -t lustre -o noatime,flock $file_system_dns_name@tcp:/fsx $directory
	sudo chown ubuntu:ubuntu ${directory}
}	


function main() {
	if [ $# -eq 0 ]; then
	    echo "FSx. No arguments supplied."
		echo "Please insert argument (--configure-and-reboot|--continue-after-reboot)"
		exit 1
	fi

	directory=$1
	file_system_dns_name=$2
	func=$3


	
	if [ $func == "--configure-and-reboot" ]; then fsx_configure_and_reboot ;  exit 1; fi
	if [ $func == "--continue-after-reboot" ]; then fsx_continue_after_reboot $directory $file_system_dns_name; exit 1; fi
	echo "Please insert argument (--configure-and-reboot|--continue-after-reboot)"
}

if [ "${1}" != "--source-only" ]; then
    main "${@}"
fi
