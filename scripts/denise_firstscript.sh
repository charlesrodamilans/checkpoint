#!/bin/bash

# This script configure ssh keys and create an nfs server
# It will mount the nfs on ~/shared director

setup_ssh_keys() {
    for hostname in $(cat hostname); do
      ssh-keyscan -H $hostname >> ~/.ssh/known_hosts
    done
}

setup_intel_mpi() {
	echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
	sudo cp limits.conf /etc/security/limits.conf
}

# Denise (IAM já com certificado e já esta configurada para intel_mpi)
setup_ssh_keys
#setup_intel_mpi


