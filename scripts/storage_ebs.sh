#!/bin/bash

directory=$1

sudo mkfs -t ext4 /dev/nvme1n1
mkdir -p $directory
sudo mount /dev/nvme1n1 $directory
sudo chown ubuntu:ubuntu $directory

