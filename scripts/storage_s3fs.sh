#!/bin/bash
directory=$1
bucket=$2

sudo apt update
sudo apt install s3fs

mkdir -p $directory
chmod 600 ${HOME}/.passwd-s3fs
s3fs $bucket $directory -o passwd_file=${HOME}/.passwd-s3fs
