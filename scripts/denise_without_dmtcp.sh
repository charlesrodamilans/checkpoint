#!/bin/bash

if [ $# -lt 3 ]; then
	echo "Invalid parameters: echo $@" 
	echo "./storage_run.sh [nprocs] [PPN] [nrep] "
	echo "or ./storage_run.sh 120 60 3"
	exit 1
fi

nprocs=$1
PPN=$2
nrep=$3

for i in $(seq 1 $nrep); do
	echo "Prepare enviroment"
	cd ~/DENISE-Black-Edition/
	rm -Rf par
	cp -Rf par-modified par
	
	# foward
	echo "Execute foward"
	cd par
	mpirun -np $nprocs -ppn $PPN  -hostfile ~/hostname  ../bin/denise DENISE_marm_OBC_foward.inp FWI_workflow_marmousi.inp > foward_out.txt 2>&1

	# fwi
	echo "Execute FWI"
	cd ~/DENISE-Black-Edition/par/su
	mkdir MARMOUSI_spike
	mv DENISE_MARMOUSI_* MARMOUSI_spike/
	cd ~/DENISE-Black-Edition/par/

	mpirun -np $nprocs -ppn $PPN  -hostfile ~/hostname ../bin/denise DENISE_marm_OBC_foward.inp FWI_workflow_marmousi.inp > fwi_out.txt 2>&1

	# cp results
	echo "Copy results"
	cd ..
	timestamp=$(date +%s)
	dir="denise_simplemodel_${nprocs}cores_${nrep}_${timestamp}"
	mkdir -p $dir
	mv par $dir/
	mkdir -p ~/denise_results
	mv $dir ~/denise_results/
#	cp $dir/ ~/efs_results/denise/
	
done





