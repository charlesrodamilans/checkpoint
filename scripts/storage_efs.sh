#!/bin/bash
	
directory=$1
efs_ip=$2

sudo apt-get update &&
sudo apt-get install -y nfs-common
mkdir -p ${directory} 
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${efs_ip}:/ ${directory}
sudo chown ubuntu:ubuntu ${directory}
#sudo echo "${efs_ip}:/ ${efs_directory} nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" >> /etc/fstab
#sudo mount -a -t efs defaults