#!/bin/bash
# apagar os arquivos
for host in $(cat ~/hostname); do 
    echo "erase_ckpt folder in $host"
    ssh -o StrictHostKeyChecking=no  ubuntu@$host "rm -Rf /home/ubuntu/ebs/ckpt"
    ssh -o StrictHostKeyChecking=no  ubuntu@$host "mkdir -p /home/ubuntu/ebs/ckpt"
done ; 

proc=64 
ppn=64
exp_path=~/efs_results/monitoring/exp_ebs_ubuntu18_$proc\_$(date +%Y%m%d%H%M%S.%3N); 
echo "exp_path=$exp_path"
mkdir -p $exp_path

# start monitoring
sar -A 1 -o $exp_path/ckpt_start_sar  >/dev/null 2>&1  & 
iostat -xmtd  1 > $exp_path/ckpt_start_iostat 2>&1 &
pidstat -urd -p ALL 1 > $exp_path/ckpt_start_pidstat 2>&1 &
sudo ~/biopattern.bpf  > $exp_path/ckpt_start_biopattern &
sudo ~/bcc/tools/biolatency.py > ckpt_start_biolatency &
sudo ~/bcc/tools/bitesize.py > ckpt_start_bitesize  &
sudo ~/bcc/examples/tracing/bitehist.py  > ckpt_start_bitehist  &
sleep 2

# clear cache and start application 
for host in $(cat ~/hostname); do ssh -o StrictHostKeyChecking=no  ubuntu@$host  'echo 3 | sudo tee /proc/sys/vm/drop_caches' ; done
dmtcp_host=$(hostname);  time dmtcp_launch -h $dmtcp_host -p 7779 --ckptdir  ~/ebs/ckpt mpirun -n $proc -ppn $ppn -hostfile ~/hostname  /home/ubuntu/NPB3.3.1/NPB3.3-MPI/bin/lu.D.$proc | tee -a $exp_path/app &   
sleep 5

# start and finish ckpt
./ckpt_time.sh start | tee -a $exp_path/ckpt_start
killall sar; killall iostat; killall pidstat; 
#sudo killall -2 biopattern.bpf bitehist.py bitesize.py bitehist.py
sudo kill -SIGINT $(pgrep -f biopattern.bpf);  sudo kill -SIGINT $(pgrep  -f  biolatency.py); sudo kill -SIGINT $(pgrep  -f bitesize.py); sudo kill -SIGINT $(pgrep  -f bitehist.py) 
sleep 2 
mv ckpt_start_* $exp_path

# start monitoring
sar -A 1 -o $exp_path/ckpt_restart_sar  >/dev/null 2>&1  & 
iostat -xmtd  1 > $exp_path/ckpt_restart_iostat 2>&1 &
pidstat -urd -p ALL 1 > $exp_path/ckpt_restart_pidstat 2>&1 &
sudo ~/biopattern.bpf  > $exp_path/ckpt_restart_biopattern &
sudo ~/bcc/tools/biolatency.py > ckpt_restart_biolatency &
sudo ~/bcc/tools/bitesize.py > ckpt_restart_bitesize  &
sudo ~/bcc/examples/tracing/bitehist.py  > ckpt_restart_bitehist  &
sleep 2



# restart
for host in $(cat ~/hostname); do ssh -o StrictHostKeyChecking=no  ubuntu@$host  'echo 3 | sudo tee /proc/sys/vm/drop_caches' ; done
./ckpt_time.sh restart | tee -a $exp_path/ckpt_restart
killall sar; killall iostat; killall pidstat;
sudo kill -SIGINT $(pgrep -f biopattern.bpf);  sudo kill -SIGINT $(pgrep  -f  biolatency.py); sudo kill -SIGINT $(pgrep  -f bitesize.py); sudo kill -SIGINT $(pgrep  -f bitehist.py) 
#sudo kill -9 $(pgrep -f biopattern.bpf);  sudo kill -9 $(pgrep  -f  biolatency.py); sudo kill -9 $(pgrep  -f bitesize.py); sudo kill -9 $(pgrep  -f bitehist.py) 
sleep 2
mv ckpt_restart_* $exp_path
# sudo killall -2 bitehist.py bitesize.py bitehist.py

echo "exp_path=$exp_path"