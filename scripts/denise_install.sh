#!/bin/bash

# dependencies
sudo apt-get -y install libfftw3-dev libfftw3-doc

git clone https://github.com/daniel-koehn/DENISE-Black-Edition

# copy marmousi Benchmark
git clone --depth=1 https://github.com/daniel-koehn/DENISE-Benchmark
cp DENISE-Benchmark/Marmousi-II/start/marmousi_II_* DENISE-Black-Edition/par/start/

# compile libs
cd DENISE-Black-Edition/libcseife
make


# phase 1 - forward_modelling_only=0

# use mpicc to compile
cd ../src
sed -i "25,30s/^/# /" Makefile  # to comment Intel MPI
sed -i "39,43s/# *//" Makefile  # to uncomment mpich
make denise

# folder to start 
cd ~/DENISE-Black-Edition/par/
~                               