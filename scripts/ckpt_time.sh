#/bin/bash

op=$1
echo "Op=$op"
ckpt='yes';

if [ $op == 'start' ]; then	cmd='dmtcp_command -c &'; fi
if [ $op == 'restart' ]; then cmd='~/ebs/ckpt/dmtcp_restart_script.sh  &';  fi

ckpt_start=$(date +%s)
eval $cmd
echo "ckpt_start_time=$(date -d @$ckpt_start +%r) - epoch=$ckpt_start"

while (true); do
    str=$(dmtcp_command -s | grep RUNNING)
    running=${str#*=}

    str=$(dmtcp_command -s | grep NUM_PEERS)
    peers=${str#*=}

    if [ $ckpt == 'yes' ] && [ $running == 'yes' ]; then
        ckpt_end=$(date +%s);
        # killall sar; killall iostat; killall pidstat; dmtcp_command -k
		dmtcp_command -k
        ckpt_time_total=$((ckpt_end-ckpt_start))
        echo "ckpt_end_time=$(date -d @$ckpt_end +%r) - epoch=$ckpt_end"
        echo "ckpt_time_total=$ckpt_time_total"
        exit
    fi
    sleep 1
done