#!/bin/bash

rm -Rf checkpoint
git clone https://charlesrodamilans@bitbucket.org/charlesrodamilans/checkpoint.git
cp checkpoint/scripts/ckpt_* ~/


mkdir -p ~/efs_results/monitoring/
for host in $(cat ~/hostname); do ssh -o StrictHostKeyChecking=no  ubuntu@$host  'mkdir -p ~/ebs/ckpt/' ; done
for host in $(cat ~/hostname); do ssh -o StrictHostKeyChecking=no  ubuntu@$host  'ls -la ~/ebs/ckpt/' ; done
dmtcp_coordinator --ckptdir ~/ebs/ckpt/ 

