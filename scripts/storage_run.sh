#!/bin/bash

function check_mounted() {
	storage=$1
	directory=$2
	if grep -qs "$directory " /proc/mounts; then
		echo "$storage already mounted."
		exit 1
	fi
	echo "$storage is not mounted. It will be installed and mounted."
}

function main() {
	if [ $# -lt 2 ]; then
		echo "Invalid parameters: echo $@" 
		echo "Please insert arguments."
		echo "./storage_run.sh [storage] [directory_mount] "
		echo "or ./storage_run.sh fsx [directory_mount] [fsx-dns-name][--configure-and-reboot|--continue-after-reboot]"
		exit 1
	fi
	
	storage=$1
	directory=$2 #"/home/ubuntu/$storage"

	
	echo "Storage: $storage" 
	
	if [ $storage == "local" ]; then
		./storage_local.sh $directory 
		exit 1
	fi	
	
	if [ $storage == "ebs" ]; then
		check_mounted $storage $directory
		./storage_ebs.sh $directory
		exit 1
	fi
	
	if [ $storage == "s3fs" ]; then
		check_mounted $storage $directory
		#bucket='charles-checkpoint'
		bucket=$3
		./storage_s3fs.sh $directory $bucket
		exit 1
	fi	
	
	if [ $storage == "efs_results" ]; then
		check_mounted $storage $directory
		efs_ip='172.31.94.184'
		./storage_efs.sh $directory $efs_ip
		exit 1
	fi	
	
	if [ $storage == "efs" ]; then
		check_mounted $storage $directory
		efs_ip=$3
		./storage_efs.sh $directory $efs_ip
		exit 1
	fi	
	
	if [ $storage == "fsx" ]; then
		 # when necessary
		check_mounted $storage $directory
		file_system_dns_name=$3
		reboot=$4
		./storage_fsx.sh $directory $file_system_dns_name $reboot
		exit 1
	fi	
	
	echo "$storage invalid. Please insert (ebs|efs|fsx|s3fs)" 
} 

if [ "${1}" != "--source-only" ]; then
    main "${@}"
fi