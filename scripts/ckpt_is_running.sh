#/bin/bash
ckpt='no'
sleep 1


while (true); do
    str=$(dmtcp_command -s | grep RUNNING)
    running=${str#*=}


    str=$(dmtcp_command -s | grep NUM_PEERS)
    peers=${str#*=}


    if [ $running == 'yes' ] && [ $peers != 0 ] ; then echo "App is running. It isn't doing ckpt.";  fi
    if [ $running == 'no' ] && [ $peers != 0 ] ; then echo "App is doing ckpt."; ckpt='yes'; fi
    if [ $running == 'no' ] && [ $peers == 0 ] ; then echo "App isn't running. It isn't doing ckpt"; fi
    if [ $running == 'yes' ] && [ $peers -lt 4 ] ; then echo "Problem. Execution finalized, but nodes didn't exit from coordinator. It will be killed."; dmtcp_command -k; exit; fi


    if [ $ckpt == 'yes' ] && [ $running == 'yes' ]; then echo "Finished ckpt"; fi
    sleep 1
done

if [ $running == 'no' ] && [ $peers == 0 ] ; then echo "App isn't running. It isn't doing ckpt"; fi
if [ $running == 'no' ] && [ $peers != 0 ] ; then echo "App is doing ckpt."; ckpt='yes'; fi
if [ $running == 'no' ] && [ $peers == 0 ] ; then echo "App isn't running. It isn't doing ckpt"; fi
if [ $ckpt == 'yes' ] && [ $running == 'yes' ]; then echo "Finished ckpt"; fi