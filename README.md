#### Create and configure enviroment

	python3 create_spot.py c5n.18xlarge us-east-1b 1.8 4 charles-fsx128
	./get_ip.py charles-fsx128 ~/conf-fsx128

	# wait (2/2 checks) - view in AWS 
	# modify the fsx-dns in ./setup.sh
	# ./setup.sh [conf_path] [storage_type] [storage ip/dns]
	./setup.sh  ~/conf-fsx128 fsx fs-0c9dd9f66aa24a8b9.fsx.us-east-1.amazonaws.com

	cat ~/conf-fsx128/public_ip
	sshaws IP

#### Execute (inside main host)
	# verify the machine
	cat hostname


	cd checkpoint
	tmux
	./dmtcp_bench_time_aws.py 128 64 3 npb_all ckp_yes 60 fsx 
	# | tee -a ~/efs_results/results/output.128cores_$(date +%s).txt
 

#### save output

prefix +  : capture-pane -S -10000
prefix +  : save-buffer output.txt
mv output.txt ~/efs_results/results/

#### download results
tar czvf results.tar.gz ~/efs_results/results/