#!/usr/bin/python3

import subprocess
import time
import re
import os
import socket
import glob
import shutil
import distutils
from distutils import dir_util
from src import npb
from src import utils
from src.dmtcp import *
from src import prepare
from src import mpi
from src import results

                        


 



##################################################


CHECKPOINT_PATH = os.path.abspath('.') + "/"

bin_path = "/home/ubuntu/NPB3.3.1/NPB3.3-MPI/bin/"
#machines = open("/home/ubuntu/hostname", "r").read().split("\n")[:-1]

file = open("/home/ubuntu/hostname", "r")
machines = [line.rstrip('\n') for line in file]
print (machines)
host = socket.gethostname()

PPN = 4 #Process per Node
ppn=4
n_vcpu = 8
checkpoint_interval = 10 
total_tests=1


ckp_local = "/home/ubuntu/checkpoint_files/"
ckp_ebs = '/home/ubuntu/ebs/checkpoint_files/'
ckp_efs = '/home/ubuntu/efs/checkpoint_files/'
ckp_s3fs = '/home/ubuntu/s3fs/checkpoint_files/'
ckp_fsx = '/home/ubuntu/fsx/checkpoint_files/'
checkpoint_path = ckp_local

storage_type = "local"

name = "results_" + storage_type + "_" +str(n_vcpu) + "cores"

#CHECKPOINT_PATH = os.path.abspath('.') + "/"
#RESULTS_PATH = CHECKPOINT_PATH + name

#RESULTS_PATH = "/home/ubuntu/efs/results/"
RESULTS_PATH = "/home/ubuntu/results/"
RESULTS_PATH = RESULTS_PATH +  name + "/"
LOGS_PATH = RESULTS_PATH + "logs/"
# EXECUTION_LOGS_PATH = LOGS_PATH # global variable (will be changed)

results_path=RESULTS_PATH
logs_path=LOGS_PATH
results_filename = RESULTS_PATH + "results.txt"



#pattern = r"^[a-z]{2}\.B\."+str(n_vcpu)
#pattern = r"lu\.B\."+str(n_vcpu)
#files = [i for i in os.listdir(bin_path) if re.match(pattern, i)]
benchmarks_files = ["ep.B.4"]
#files = ["mg.B.4"]

#mpi.run_experiments_with_checkpoint(total_tests, machines, n_vcpu, ppn, checkpoint_interval, results_filename, ckptdir, bin_path, files, results_path, logs_path)
 
mpi.run_experiments_with_checkpoint(total_tests, machines, n_vcpu, ppn, bin_path, benchmarks_files, logs_path, results_filename, 
                                    results_path, host, checkpoint_interval, checkpoint_path)    
#mpi.run_experiments_without_checkpoint(total_tests, machines, host, n_vcpu, ppn, interval, results_filename, bin_path, files, results_path, logs_path)
# def run_experiments_without_checkpoint(total_tests, machines, host, n_vcpu, ppn, interval, results_filename, \
#                                         files,results_path, logs_path):

