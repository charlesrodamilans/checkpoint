#!/usr/bin/python3

import subprocess
import time
import re
import os
import socket
import statistics
import glob
import shutil
import distutils
#from distutils import dir_util
from src import npb
import time
import sys

def dmtcp_running():
        cmd = "dmtcp_command -s"
        txt = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf-8')
        pattern=r"NUM_PEERS=(.*)\s*RUNNING=(.*)"
        found = re.findall(pattern, txt)
        print (str(found))
        try:
                t = found[0][0]
        except IndexError:
                return 0 # kill dmtcp with 'dmtcp_command -q'

        if int(found[0][0]) <= 3:
                return 0 # kill dmtcp with 'dmtcp_command -q'
        elif found[0][1] == "yes":
                return 1 # running normal
        # else:
#             return 2 # program is not running (checkpointing, restarting or finished)
        elif found[0][1] == "no" and int(found[0][0]) > 0:
                return 2 # program is not running (checkpointing, restarting or finished))
        else:
                return 3 # only dmtcp_cordinator running

def main():
    start = time.time()
    while True:
        
        run = dmtcp_running()
        print(time.strftime("[%Y/%m/%d %H:%M:%S]"), "Running Status: ", run, )    
        
        if (run == 1): #  [('7', 'no')]
            # print("dmtcp_running(): " + str(dmtcp_running()))
            break
        time.sleep(.5)
            


    end = time.time() 
    print("Total time: ", end-start)
        


if __name__ == '__main__':
    main()
    # dmtcp_running()