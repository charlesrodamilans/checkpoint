#!/usr/bin/python
# Copyright 2018 Nicholas Torres Okita <nicholas.okita@ggaunicamp.com>

import boto3
import base64
from botocore.exceptions import ClientError
import sys
from datetime import datetime
import time

from logging import getLogger
import logging


def main():
    subnets = {
        'us-east-1a': 'subnet-bbe700dc',
        'us-east-1b': 'subnet-75555a5a',
        'us-east-1c': 'subnet-9bf928d1',
        'us-east-1d': 'subnet-53f8f50e',
        'us-east-1e': 'subnet-38ecdc07',
        'us-east-1f': 'subnet-3343f63c',
    }
    
    placement_group = {
        'us-east-1a': 'ckpt-cluster-a',
        'us-east-1b': 'ckpt-cluster-b',
        'us-east-1c': 'ckpt-cluster-c',
        'us-east-1d': 'ckpt-cluster-d',
        'us-east-1e': 'ckpt-cluster-e', # c5n instance isn't work in us-east-1e
        'us-east-1f': 'ckpt-cluster-f',
    }
    user_data = """#!/bin/bash

"""

    if len(sys.argv) <= 1:
        print('Please insert instance type')
        print("Example: python3 create_spot.py c5n.large us-east-1b 0.1 1 charles-local")
        print("Example: python3 create_spot.py c5n.18xlarge us-east-1b 1.8 2 charles-fsx4")
        return

    instance_type_in = sys.argv[1]

    ec2 = boto3.client('ec2', region_name='us-east-1')
    conn = ec2
    # zones = conn.describe_availability_zones()
    zoneNames = [ 'us-east-1a' ]
    nInstances = 1
    tagName="charles"

    bestZone = ['', sys.float_info.max]
    for zone in zoneNames:
        history = conn.describe_spot_price_history(
                StartTime=datetime.now().isoformat(),
                EndTime=datetime.now().isoformat(),
                ProductDescriptions=['Linux/UNIX'],
                InstanceTypes=[instance_type_in],
                AvailabilityZone=zone);
        for h in history['SpotPriceHistory']:
            if float(h['SpotPrice']) < bestZone[1]:
                bestZone[0] = h['AvailabilityZone']
                bestZone[1] = float(h['SpotPrice'])

    if len(sys.argv) == 6:
        bestZone[0] = sys.argv[2]
        bestZone[1] = float(sys.argv[3])
        nInstances = int(sys.argv[4])
        tagName=sys.argv[5]
        

    price = bestZone[1]
    PRICE = str(price*1.2)

#    logger.info("Instance " + instance_type_in + " in " + bestZone[0] + " for " + str(price))
    print("Instance " + instance_type_in + " in " + bestZone[0] + " for " + str(price) + \
             " ;  " + str(nInstances) + " instances" + " with tagName " + tagName)

    response  = ec2.request_spot_instances(
            SpotPrice=PRICE,
            InstanceCount=nInstances,
            LaunchSpecification={
                'InstanceType': instance_type_in,
                #'ImageId': 'ami-0b8ffb06fc996bae7', # ami devito mpi
                # 'ImageId': 'ami-053a938fb45f1a766', #ami npb - ebs with 100 GB
                 'ImageId': 'ami-084402cfe9759506a', #ami npb and ckpt scripts- ebs with 100 GB
                # 'ImageId': 'ami-05906babb8aeb1181', #ami denise - ebs with 100 GB
                # 'ImageId': 'ami-09ccb835f345dca5a', #ami denise - ubntu 18.04 - bcc - ebs with 100 GB
                
                
                
                'SecurityGroupIds': ['sg-0ac954d0a8d29e49b'],
                'SubnetId': subnets[bestZone[0]],
                'UserData': (base64.b64encode(user_data.encode())).decode(),
                'KeyName': 'charles-cces',
                'Monitoring': {
                    'Enabled': False
                },
                'Placement': {
                    'AvailabilityZone': bestZone[0],
                    'GroupName': placement_group[bestZone[0]] # 'checkpoint_cluster'
                },
                'BlockDeviceMappings': [
                    {
                        'DeviceName': '/dev/sda1',
                        'VirtualName': 'eth0',
                        'Ebs': {
                            'DeleteOnTermination': True,
                            'VolumeSize': 100,
                            'VolumeType': 'gp2',
                        },
                        'NoDevice': ''
                    },
                    {
                        "DeviceName": "/dev/sdb",
                        "VirtualName": "checkpoint",
                        "Ebs": {
                            "DeleteOnTermination": True,
                            "VolumeSize": 100,
                            "VolumeType": "gp2"
                        },
                        'NoDevice': ''
                    },
                ],
            })

     
    # spot_request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']
  #   print('Spot Request done! Spot Request Id: '+spot_request_id)

    for  i in range(0,nInstances):
        spot_request_id = response['SpotInstanceRequests'][i]['SpotInstanceRequestId']
        print('Spot Request done! Spot Request Id: '+spot_request_id)

    print("Waiting...")
    time.sleep(30)
   
    print("Tag Instances")
    for  i in range(0,nInstances):
        
        spot_request_id = response['SpotInstanceRequests'][i]['SpotInstanceRequestId']
        
        responseSpotResquestId = ec2.describe_spot_instance_requests(SpotInstanceRequestIds=[spot_request_id])
        cur_spot = responseSpotResquestId['SpotInstanceRequests'][0]
        
        # print('Spot Request Id: '+spot_request_id)
        if ('InstanceId' in cur_spot):
            print('Spot Request Id: '+spot_request_id +', Spot Instance Id: ' + cur_spot['InstanceId'])
            result = ec2.create_tags(Resources=[cur_spot['InstanceId']],
                    Tags=[
                        {
                            'Key': 'type',
                            'Value': 'spot',
                        },
                        {
                            "Key": "Name",
                            "Value": tagName,
                        },
                        {
                            "Key": "project",
                            "Value": "checkpoint"
                        },
                        ]
                    )
        else:
            print('Spot Request for ' + instance_type_in + ' failed in zone ' + bestZone[0])
            ec2.cancel_spot_instance_requests(SpotInstanceRequestIds=[spot_request_id])


if __name__ == '__main__':
    logger = getLogger(__name__)
    # fh = logging.FileHandler('spot.log')
  #   fh.setLevel(logging.DEBUG)
  #   fh.setLevel(logging.ERROR)
  #   fh.setLevel(logging.INFO)
  #   logger.addHandler(fh)

    main()
