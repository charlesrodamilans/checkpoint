#!/bin/bash

aws_keypem=~/.aws/charles-cces.pem

function fsx_before_reboot() {
	fsx_dns=$1
	for hostname in $(cat $public_ip_path); do
	  echo "fsx before-reboot. Copy files in: $hostname"
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_run.sh ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_fsx.sh ubuntu@$hostname:~/
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh $storage /home/ubuntu/$storage $fsx_dns --configure-and-reboot"
	done
}

function fsx_after_reboot() {
	fsx_dns=$1
	echo "Waiting reboot (60 s)"; sleep 30; echo "Waiting reboot (30 s)";sleep 30;
	for hostname in $(cat $public_ip_path); do
	  echo "Fsx --continue-after-reboot in: $hostname"
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh $storage /home/ubuntu/$storage $fsx_dns --continue-after-reboot"
	  echo ""
	done
	echo ""
}


function ssh_mpi() {
	for hostname in $(cat $public_ip_path); do
	  echo "Copy files in: $hostname"
	  tmp="$conf_path/*"
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem $tmp ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/limits.conf ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/firstscript.sh ubuntu@$hostname:~/
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname  './firstscript.sh'
	done
}

# machine host
function main_host() {
	hostname=$(head -n 1 $public_ip_path)
	echo "Prepare main host: $hostname"
	ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "
	"
} 


function print_mounted() {
	for hostname in $(cat $public_ip_path); do
	  echo "Mounted in: $hostname"
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "df -h"
	done
}

function install_efs() {
	efs_ip=$1
	for hostname in $(cat $public_ip_path); do
	  echo "EFS install in: $hostname"
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_efs.sh ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_run.sh ubuntu@$hostname:~/
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh efs /home/ubuntu/efs $efs_ip"
	done
}

function install_s3fs() {
	s3sf_bucket=$1
	for hostname in $(cat $public_ip_path); do
	  echo "EFS install in: $hostname"
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ~/.passwd-s3fs ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_s3fs.sh ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_run.sh ubuntu@$hostname:~/
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh s3fs /home/ubuntu/s3fs $s3sf_bucket"
	done
}

function install_ebs() {
	for hostname in $(cat $public_ip_path); do
	  echo "EBS install in: $hostname"
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_ebs.sh ubuntu@$hostname:~/ 
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_run.sh ubuntu@$hostname:~/ 
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh ebs /home/ubuntu/ebs"
	done
}

function install_ssd() {
	for hostname in $(cat $public_ip_path); do
	  echo "EBS install in: $hostname"
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_ebs.sh ubuntu@$hostname:~/
	  scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_run.sh ubuntu@$hostname:~/
	  ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh ebs /home/ubuntu/ssd"
	done
}

function all_hosts() {
	for hostname in $(cat $public_ip_path); do
		echo "EBS results in: $hostname"
	  	scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_run.sh ubuntu@$hostname:~/
	  	scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage_efs.sh ubuntu@$hostname:~/
	  	ssh -o StrictHostKeyChecking=no  -i  $aws_keypem ubuntu@$hostname "./storage_run.sh efs_results /home/ubuntu/efs_results"
	done
}


if [ $# -lt 2 ]; then
	echo "./setup.sh [conf_path] [storage_type] [storage ip/dns]"
	exit 1
fi

conf_path=$1
storage=$2

public_ip_path="$conf_path/public_ip"
echo "conf_path: $conf_path"
echo "Storage: $storage"
echo "public_ip_path: $public_ip_path"

echo "Public IP:"
echo "$(cat $conf_path/public_ip)"
echo ""


if [ $storage == "fsx" ]; then
	#fsx_dns='fs-0fc5c7242b9c92822.fsx.us-east-1.amazonaws.com' # 128 cores
	#fsx_dns='fs-09f7384bc30e1a9b4.fsx.us-east-1.amazonaws.com' # 256 cores
	#fsx_dns='fs-0093a8a49e96a3e47.fsx.us-east-1.amazonaws.com' #512
	fsx_dns=$3
	echo "fsx_dns: $fsx_dns"
	# fsx_before_reboot $fsx_dns
	# fsx_after_reboot $fsx_dns
	ssh_mpi
	main_host
	all_hosts
	print_mounted 
fi	

if [ $storage == "efs" ]; then
	efs_ip=$3
	echo "efs_ip: $efs_ip"
	install_efs $efs_ip
	ssh_mpi
	main_host
	all_hosts
	print_mounted 
fi	

if [ $storage == "s3fs" ]; then
	s3sf_bucket=$3
	echo "s3fs_bucket: $s3sf_bucket"
	install_s3fs $s3sf_bucket
	ssh_mpi
	main_host
	all_hosts
	print_mounted 
fi


if [ $storage == "ssd" ]; then
	echo "SSD Install"
	install_ssd $ssd_path
	ssh_mpi
	main_host
	all_hosts
	print_mounted
fi

if [ $storage == "ebs" ]; then
	echo "EBS Install"
	install_ebs $ssd_path
	ssh_mpi
	main_host
	all_hosts
	print_mounted
fi

if [ $storage == "local" ]; then
	echo "Local Install"
	ssh_mpi
	main_host
	all_hosts
	print_mounted
fi

# #scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ../scripts/storage* ubuntu@$hostname:~/
# #scp -o StrictHostKeyChecking=no  -r -i  $aws_keypem ~/.passwd-s3fs ubuntu@$hostname:~/

# git config --global credential.helper store ; git clone https://charlesrodamilans@bitbucket.org/charlesrodamilans/checkpoint.git

