#!/usr/bin/python3
import boto3
import base64
import sys
import os

def main():
    if len(sys.argv) <= 2:
        print('Please insert tag Name and conf path')
        return

    # conf_path = "conf/"
    tag_name = sys.argv[1]
    conf_path =  sys.argv[2]
    
    print ("tag Name: " + tag_name + ", conf path: " + conf_path)
    
    try:
        # Create target Directory
        os.mkdir(conf_path)
        print("Directory " , conf_path ,  " Created ") 
    except FileExistsError:
        print("Directory " , conf_path ,  " already exists")

    
    path_public_ip = conf_path + "/public_ip"
    path_private_ip = conf_path  + "/private_ip"
    path_hostname = conf_path  + "/hostname"
        
    ec2 = boto3.resource('ec2', region_name='us-east-1')
    
    filters = [{'Name': 'instance-state-name', 'Values': ['running']}, 
                {'Name': 'tag:Name', 'Values': [tag_name]}]
    instance_collection = ec2.instances.filter(Filters=filters)
    instances = [_ for _ in instance_collection]

    
    print("Public IP:")    
    with open(path_public_ip, "w") as f:    
        for ins in instances:
            print(ins.public_ip_address)
            f.write(ins.public_ip_address+'\n')

    print("\nPrivate IP:")
    with open(path_private_ip, "w") as f:
        for ins in instances:
            print(ins.private_ip_address)
            f.write(ins.private_ip_address+'\n')

    print("\nHostname:")
    with open(path_hostname, "w") as f:
        for ins in instances:
            hostname = ins.private_dns_name.replace('.ec2.internal', '')
            print(hostname)
            f.write(hostname+'\n')

   #  print("\nPrivate Tags:")
   #  for ins in instances:
   #      print(ins.tags)

    

if __name__ == '__main__':
    main()